#!/bin/bash
name=Giri
#function1 declaration
function function1(){
    echo "Hello $name! This is function one"
    function2 #calls function2()
}

function function2(){
    echo "Hello $name! This is functoin two"
}

function main(){
    echo "Hello $name! This is the main function"
    function1 #calls function1()
}

#calling main function
main