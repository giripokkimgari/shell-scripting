#!/bin/bash

HOST="www.google.com"

ping -c 2 $HOST

if [ "$?" -eq "0" ]
then
   echo ""
    echo "host $HOST is reachable"
else

   echo "host is not reachable"
fi