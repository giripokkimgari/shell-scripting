#!/bin/bash
#shebang opeartor tells the system to use bash interpreter for this scirpt execution
set -x

echo "This shell script prints multiplication table"
echo "Enter table number: "
table=$1
set +x
for range in {1..10}
do
 result=$(($table * $range))
 echo  "$table x $range = $result"
done


#############
echo "-------------"
#!/bin/bash

_DEBUG="on"
function DEBUG()
{
 [ "$_DEBUG" == "on" ] &&  $@
}
 
DEBUG echo 'Reading files'
for i in *
do
  grep 'something' $i > /dev/null
  [ $? -eq 0 ] && echo "Found in $i file"
done
DEBUG set -x
a=2
b=3
c=$(( $a + $b ))
DEBUG set +x
echo "$a + $b = $c"