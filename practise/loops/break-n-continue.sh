#!/bin/bash

#break statement
for i in {1..10}
do 
  echo "current number is $i"
  if [ $i == 4 ]
  then
      echo "Condition met! Exiting the loop using break statement"
  break
  fi
  echo "Condition didn't match"
done

echo -e "\n--------------\ns"

#continue statement

for i in {1..10}
do 
  echo "current number is $i"
  if [ $i == 4 ]
  then
      echo "Condition met! Exiting the loop using break statement"
  continue
  fi
  echo "Condition didn't match"
done


echo "---------"

echo "This is a test message from Visual Studio Code!!" | logger

logger "This is a second message"

#cat /var/log/messages